// Import from submodule to get smaller bundle size without having to set up
// treeshaking.
import concat from 'lodash/concat';

// Note that even though not importing "getDouble", it is still included in the
// bundle, because "getNum" and "getDouble" are not implemented as individual
// modules like Lodash (see above).
import { getNum } from './utils';

const combinedArray = concat([getNum(1), 2], 3);

console.log('The combined array is:', combinedArray);
