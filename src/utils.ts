/**
 * Gets a number.
 * @param n The number to be returned.
 */
function getNum(n: number): number {
    return n;
}

/**
 * Doubles a number.
 * @param n The number to double.
 */
function getDouble(n: number): number {
    return n * 2;
}

export { getNum, getDouble };
