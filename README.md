# typescript-for-browser-example

Shows using TypeScript to create a JS bundle with the ability to step through the raw TS in Chrome debugging.

![screenshot of Chrome debugging TypeScript code](img/example.png)

## webpack

Webpack *must* be used to compile the TypeScript, in order to preserve the source maps the whole way through. If you want to use a build tool like Gulp, *don't* have Gulp run the TypeScript compiler and Webpack separately. Have Gulp run Webpack and have Webpack encapsulate the TypeScript compile.

To get the best source maps, the `"sourceMap": true` property is set in the `tsconfig.json` file and the `devtool: "inline-source-map"` property is set in the `webpack.config.js` file.

## running the example

Run `npm run build` to build the bundle into the `dist/public/scripts` directory. Run `npm run serve` to run the build automatically and then serve the `index.html` web page that uses it on `http://localhost:8080`.
